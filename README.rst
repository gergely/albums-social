Albums.social
#############

Federated photo album sharing for everyone.

-----

!WARNING! This software, including this document, is under active development, and is not intended
for production use!

Running the application locally
-------------------------------

Albums.social uses Pipenv to manage dependencies.  To install it, use::

  $ pip install pipenv

See the `Pipenv documentation <https://docs.pipenv.org/>`_ for details.

After Pipenv is installed, you can install the dependencies with::

  $ pipenv install

And run the application with::

  $ pipenv run gunicorn app:app

This will boot up the applicatio on https://127.0.0.1:8000/

Hacking
-------

If you want to hack on the sources, just go ahead and edit the `.py` files and restart the application.

Contributing
------------

To contribute to the project, go to the `project page <https://gitlab.com/gergelypolonkai/albums-social>`_, open issues, send merge requests, and all the usual stuff.

License
-------

The project is licensed under the GNU Affero General Public License.  See the LICENSE.txt file, or
the `online version <https://www.gnu.org/licenses/agpl-3.0.en.html>`_ for details.

Code of Conduct
---------------

We use the `Contributor Covenant <https://www.contributor-covenant.org/version/1/4/code-of-conduct>`_.  Please act accordingly.
